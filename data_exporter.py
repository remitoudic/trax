import sqlite3
import epg_parser

data_to_export= epg_parser.DATA


class DB_insertion : 

    def __init__ (self):
       self.con = self.sql_connection()
       self.cur= self.con.cursor() 
    
    def sql_connection(self):
        try:
            con = sqlite3.connect('epg.db')
            return con
        except sqlite3.Error:
            print(sqlite3.Error)

    def sql_insert(self, my_dict, table_name):
        """  transfer dict to the db
             for thecasting table add keys personid ,  programid  and their values
        """
        if table_name == "casting":
            my_dict[2]["personid"]  = self.get_personid(my_dict[1])
            my_dict[2]["programid"] = self.get_programid(my_dict[0]) 
            print(my_dict[2])
            my_dict= my_dict[2]
        
        columns = ', '.join(my_dict.keys())
        placeholders = ':'+', :'.join(my_dict.keys())
        query = "INSERT INTO " + table_name +" (%s) VALUES (%s)" % (columns, placeholders)
        
        self.cur.execute(query, my_dict)
        self.con.commit()
        
    def get_personid( self,my_dict):
        " to get the get_personid "
   
        first_name = my_dict["firstname"]
        lastname= my_dict["lastname"]

        query = """ SELECT personid FROM  person 
                    WHERE firstname = "{}" 
                    AND    lastname  = "{}";
                """.format(first_name,lastname)

        self.cur.execute(query)
        return self.cur.fetchone()[0]


    def get_programid(self, my_dict ) :
        "to get get_programid"
        title= my_dict["title"]
        duration = my_dict["duration"]
        
        query = """ SELECT programid FROM program
                    WHERE title = "{}" 
                    AND duration ='{}';
            """.format(title, duration )
        
        self.cur.execute(query)
        return self.cur.fetchone()[0]
    
    
    def insert_program_perso_casting(self,program_number):
        """ take care of the all transfer of one program """
        self.sql_insert(data_to_export["all_programs"][program_number], "program")
        self.sql_insert(data_to_export["all_persons"][program_number], "person")
        self.sql_insert((data_to_export["all_programs"][program_number],
                data_to_export["all_persons"][program_number],
                data_to_export["all_castings"][program_number]), "casting")
        print("in", program_number)


    def loop_insert_prosses(self):
        for prog_number in range (len(data_to_export["all_programs"] )):
            self.insert_program_perso_casting(prog_number)
        

    def clean_duplicate(self):

        sql = """"DELETE FROM  person 
                  WHERE  rowid NOT IN (
                      SELECT min(rowid) FROM person 
                                        GROUP BY firstname , lastname );"""
        self.cur.execute(sql)
        self.con.close()


  
#if __name__ == "__main__":
#    exporter = DB_insertion()
#    exporter.loop_insert_prosses()
#    exporter.clean_duplicate()