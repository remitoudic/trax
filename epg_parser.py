import requests
import xml.etree.ElementTree as ET
import config 


class EPG_parser:

    def __init__(self): 
        self.url = config.config["url"]
        self.cafile = config.config["SSL_file"]
        self.xml_to_parse =  None
        self.all_programs =  list() 
        self.all_persons = list() 
        self.all_castings = list() 
       
    def fetch_data(self): 
        request = requests.get(self.url, verify=self.cafile)
        self.xml_to_parse =  request.text

    def parse_data (self): 
        root = ET.fromstring(self.xml_to_parse)
    
        for element in root:
        
            # For table program  
            program = dict()
            program["start_time"] = element.attrib["start_time"]
            program["title"]= element[1].text
            program["subtitle"] = element[2].text
            program["duration"] = element[0].text
            program["Type"] =  element[3].text
            program["description"] =  element[4].text
            self.all_programs.append(program)
            
            # For table person
            person, casting = dict(), dict()
    
            # condition if no data 
            if len(element[5])> 0 :
                # more than one actor in program 
                person["firstname"]=""
                person["lastname"]= ""
                casting["function"]= ""
                for child in  range(len(element[5])): 
                    
                    #for table person 
                    person["firstname"] = person["firstname"] + element[5][child].attrib["firstname"] + "," 
                    person["lastname"]  = person["lastname"]  + element[5][child].attrib["lastname"] + "," 
                
                    #  For table casting 
                    casting["function"] =casting["function"] + element[5][child].attrib["function"] + "," 
            
             #  if no data    
            else:
            
                person["firstname"]= "unknow"
                person["lastname"] = "unknow"
                casting["function"]= "unknow" 
            
            self.all_persons.append(person)
            self.all_castings.append(casting)
    
    def data_to_export(self): 
        data_to_export=dict()
        data_to_export["all_programs"]= self.all_programs
        data_to_export["all_castings"]= self.all_castings
        data_to_export["all_persons"]= self.all_persons
        return data_to_export
        

    def parsing_process(self): 
        self.fetch_data()
        self.parse_data()
        self.data_to_export()

    

EPG= EPG_parser () 
EPG.parsing_process()
DATA= EPG.data_to_export()

print( "all good")