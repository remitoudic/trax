# CREATE  DATA SCHEMA    

import sqlite3

class EPG_data_schema:

    def __init__ (self):
        self.con = self.sql_connection()
        self.cur= self.con.cursor()
        self.sheet = None
    
    def sql_connection(self):
        try:
            con = sqlite3.connect('epg.db')
            return con
        except sqlite3.Error:
            print(sqlite3.Error)
       
    def create_table_program(self):
        sql_create= """
       
        CREATE TABLE IF NOT  EXISTS program(
            programid integer PRIMARY KEY,
            start_time datetime,
            title text, 
            subtitle text, 
            duration int, 
            Type text, 
            description text);  """
                 
        self.cur.execute(sql_create)
        self.con.commit()
    
    def create_table_person(self):
        sql_create= """ 
      
        CREATE TABLE IF NOT  EXISTS person(
            personid  integer PRIMARY KEY,
            firstname text, 
            lastname text );  """
        self.cur.execute(sql_create)
        self.con.commit()
                     
    def create_table_casting(self):
        sql_create= """ 
      
        CREATE TABLE IF NOT  EXISTS casting(
            castingid integer PRIMARY KEY,
            personid int,
            programid int,
            function int , 
            FOREIGN KEY(personid) REFERENCES person(personid),
            FOREIGN KEY(programid) REFERENCES program(programid));  """
                     
        self.cur.execute(sql_create)
        self.con.commit()
        
    
    def create_schema(self): 
        test.clean_all()
        self.create_table_program()
        self.create_table_casting()
        self.create_table_person()
    

    def closed_con(self):
        self.con.close()
        
    def clean_all(self):
        sql= """ 
        select 'drop table ' || name || ';' from sqlite_master
        where type = 'table';
        """
        self.cur.execute(sql)
        self.con.commit()

        
#########      
test = EPG_data_schema()
test.clean_all()
test.create_schema()
test.closed_con()

print ( "all good ")